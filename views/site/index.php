<?php
    use yii\grid\GridView;
/** @var yii\web\View $this */

$this->title = 'My Awesome Exam';
?>
<div class="site-index">
    <h1 class="centrarTexto">MY-AWESOME-EXAM</h1>
    <div class="fondo">
        <div class="card-title">
            <h1 class="separarTexto"><?=$tituloC2?></h1>
        </div>
        <div class="card-body">
            <h3><?=$subtituloC2?></h3>
        </div>
    </div>
    <?= GridView::widget([
    'dataProvider' => $resultadoC1,
    'columns' => $camposC1,
    'headerRowOptions' => ['class' => 'centrarTexto'],
    'rowOptions' => ['class' => 'centrarTexto'],
    'layout' => "{items}",
    ]); ?>
    
    <div class="fondo altura">
        <div class="card-title">
            <h1 class="separarTexto"><?=$tituloC2?></h1>
        </div>
        <div class="card-body">
            <h3><?=$subtituloC2?></h3>
        </div>
    </div>
    <?= GridView::widget([
    'dataProvider' => $resultadoC2,
    'columns' => $camposC2,
    'headerRowOptions' => ['class' => 'centrarTexto'],
    'rowOptions' => ['class' => 'centrarTexto'],
    'layout' => "{items}",
    ]); ?>
    
    <div class="fondo altura">
        <div class="card-title">
            <h1 class="separarTexto"><?=$tituloC3?></h1>
        </div>
        <div class="card-body">
            <h3><?=$subtituloC3?></h3>
        </div>
    </div>
    <?= GridView::widget([
    'dataProvider' => $resultadoC3,
    'columns' => $camposC3,
    'headerRowOptions' => ['class' => 'centrarTexto'],
    'rowOptions' => ['class' => 'centrarTexto'],
    'layout' => "{items}",

    ]); ?>

</div>
