<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Puerto;
use app\models\Etapa;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //Consulta 1 del examen 
        $consulta1 = new ActiveDataProvider([
            'query' => Puerto::find()->select('nompuerto, dorsal')->distinct()->where('altura<1000'),
        ]);
        
        //Consulta 2 del examen
        $consulta2 = new ActiveDataProvider([
            'query' => Etapa::find()->select('numetapa, kms')->distinct()->where('salida=llegada'),
        ]);
        
        //Consulta 3 del examen
        $consulta3 = new ActiveDataProvider([
            'query' => Puerto::find()->select('nompuerto, dorsal')->distinct()->where('altura>1500'),
        ]);
        
      
        return $this->render('index',[
            'resultadoC1' => $consulta1,
            'camposC1' => ['nompuerto', 'dorsal'],
            'tituloC1' => 'Consulta 1 ORM',
            'subtituloC1' => 'Nombre del puerto y dorsal ganador de los puertos con altura menor a 1000.',
            
            'resultadoC2' => $consulta2,
            'camposC2' => ['numetapa', 'kms'],
            'tituloC2' => 'Consulta 2 ORM',
            'subtituloC2' => 'Distancia y número de las etapas circulares.',
            
            'resultadoC3' => $consulta3,
            'camposC3' => ['nompuerto', 'dorsal'],
            'tituloC3' => 'Consulta 3 ORM',
            'subtituloC3' => 'Nombre del puerto y dorsal ganador de los puertos con altura mayor a 1500.',
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    } 
}
